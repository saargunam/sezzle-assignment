# DEPLOY A WEBSERVER CLUSTER USING THE WEBSERVER-CLUSTER MODULE

terraform {
  required_version = ">= 0.12.26"
  backend "s3" {
    encrypt = true
    bucket = "terraform-state-sezzle-example"
    dynamodb_table = "terraform-locks"
    key    = "stage/services/webserver-cluster/web.tfstate"
    region = "us-east-2"
  }
}

provider "aws" {
  region = "us-east-2"
}

# DEPLOY THE WEBSERVER-CLUSTER MODULE

module "webserver_cluster" {
  source = "../../../../modules/services/webserver-cluster"

  cluster_name  = "webservers-prod"
  instance_type = "t3.large
  min_size      = 2
  max_size      = 3
}
