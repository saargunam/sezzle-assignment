Hello world app provisioned via Terraform in AWS

# Terraform:

- Provisioning the infra for Highly available webserver cluster

       - Autoscaling group for web servers
       - ELB [ for loadbalancing between web servers]
       - Health checks enabled 
       - S3 to store terraform statefiles
       - DynamoDB for statefile locking


- File layout of terraform files :

       ├── live_infra
       │   ├── prod
       │   │   └── services
       │   │       └── webserver-cluster
       │   │           ├── main.tf
       │   │           └── outputs.tf
       │   └── stage
       │       └── services
       │           └── webserver-cluster
       │               ├── main.tf
       │               └── outputs.tf
       ├── modules
       │   └── services
       │       └── webserver-cluster
       │           ├── main.tf
       │           ├── outputs.tf
       │           └── variables.tf

- Deployment is done via GitlabCI pipeline


# Reference architecture :

![](arch-diagram/AWS-Arch.png)%
